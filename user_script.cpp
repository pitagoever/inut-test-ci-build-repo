#include "user_script.h"
#include "led.h"

Led led;

void UserScript::setup() {
	Serial.begin(115200);
	Serial.println("OK, i'm comming from user_script");
	led.setup();
}

void UserScript::loop() {
	led.loop();
	
	static uint32_t lastPrint = millis();
	
	if ((uint32_t)(millis() - lastPrint) > 1000UL) {
		lastPrint = millis();
		Serial.println("Loop call me");
	}
}